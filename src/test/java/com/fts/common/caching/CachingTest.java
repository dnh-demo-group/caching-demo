package com.fts.common.caching;

import com.fts.common.WalletData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class CachingTest {

    private static Caching<WalletData> caching;

    @Before
    public void init() {
        DataObject<WalletData> dataObject = new WalletData();
        caching = new LFU<>(dataObject, 100);
        caching.insert(1L, new WalletData(1L, BigDecimal.TEN, BigDecimal.ONE));
        caching.insert(2L, new WalletData(2L, BigDecimal.TEN, BigDecimal.valueOf(2L)));
        caching.insert(3L, new WalletData(3L, BigDecimal.TEN, BigDecimal.valueOf(3L)));
        caching.insert(4L, new WalletData(4L, BigDecimal.TEN, BigDecimal.valueOf(4L)));
        caching.insert(5L, new WalletData(5L, BigDecimal.TEN, BigDecimal.valueOf(5L)));
        caching.insert(5L, new WalletData(5L, BigDecimal.TEN, BigDecimal.valueOf(5L)));
        caching.insert(2L, new WalletData(2L, BigDecimal.TEN, BigDecimal.valueOf(2L)));
        caching.insert(3L, new WalletData(3L, BigDecimal.TEN, BigDecimal.valueOf(3L)));
        caching.insert(4L, new WalletData(4L, BigDecimal.TEN, BigDecimal.valueOf(4L)));
        caching.insert(5L, new WalletData(5L, BigDecimal.TEN, BigDecimal.valueOf(5L)));
        caching.insert(5L, new WalletData(5L, BigDecimal.TEN, BigDecimal.valueOf(5L)));
    }

    @Test
    public void insert_over_cachingSize_then_remove_old_data() {
        caching.insert(6L, new WalletData(6L, BigDecimal.TEN, BigDecimal.valueOf(6L)));
//        Assert.assertNull(caching.read(1L));

        Assert.assertNotNull(caching.read(2L));
        Assert.assertNotNull(caching.read(3L));
        Assert.assertNotNull(caching.read(4L));
        Assert.assertNotNull(caching.read(5L));
        Assert.assertNotNull(caching.read(6L));

        WalletData wallet =  caching.read(2L);
        Assert.assertEquals("Should be 2",2L, wallet.getWalletId().longValue());
    }

    @Test
    public void read_then_update_caching_then_insert_oversize() {
        WalletData wallet =  caching.read(1L);
        caching.write(wallet.getWalletId());
        caching.insert(6L, new WalletData(6L, BigDecimal.TEN, BigDecimal.valueOf(6L)));
        Assert.assertNotNull(caching.read(1L));
    }

    @Test
    public void read_then_update_caching() {
        WalletData wallet =  caching.read(1L);
        wallet.setBalance(wallet.getBalance().add(BigDecimal.valueOf(12L)));
        caching.write(wallet.getWalletId());
        wallet.setBalance(wallet.getBalance().add(BigDecimal.valueOf(13L)));
        caching.write(wallet.getWalletId());
        WalletData wallet2 =  caching.read(1L);
        Assert.assertNotNull(wallet2);
        Assert.assertEquals("should be 35", BigDecimal.valueOf(35), wallet2.getBalance());
    }

    @Test
    public void update_then_remove_least_used() {
        caching.write(1L);
        caching.write(2L);
        caching.write(3L);
        caching.write(4L);
        caching.insert(6L, new WalletData(6L, BigDecimal.TEN, BigDecimal.valueOf(6L)));
//        Assert.assertNull(caching.read(5L));
    }
}
