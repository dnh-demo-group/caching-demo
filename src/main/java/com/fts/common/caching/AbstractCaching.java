package com.fts.common.caching;


public abstract class AbstractCaching<T> implements Caching<T> {
    protected int cacheSize = 16;
    protected int currentSize = 0;
    private int hitCount;
    private int missCount;
    protected final DataObject<T> dataObject;
    protected final Object[] cache;

    public AbstractCaching(DataObject<T> dataObject) {
        this.dataObject = dataObject;
        cache = new Object[this.cacheSize];
        fill(dataObject);
    }

    public AbstractCaching(DataObject<T> dataObject, int cacheSize) {
        this.dataObject = dataObject;
        this.cacheSize = cacheSize;
        cache = new Object[cacheSize];
        fill(dataObject);
    }

    private void fill(DataObject<T> dataObject) {
        for (int i = 0; i < this.cacheSize; i++) {
            cache[i] = dataObject.newInstance();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public T read(Long key) {
        int idx = doRead(key);
        if (idx == -1) {
            missCount++;
            return null;
        }
        hitCount++;
        return (T) cache[idx];
    }

    @Override
    @SuppressWarnings("unchecked")
    public void insert(Long key, T t) {
        int idx;
        if ((idx = doRead(key)) != -1) {
            //Key exists
            doWrite(key);
            dataObject.set((T) cache[idx], t);
            return;
        }
        //Insert if full
        if (currentSize >= cacheSize) {
            //Remove use cache strategy
            idx = doRemove();
            doInsert(key, idx);
            dataObject.set((T) cache[idx], t);
        } else {
            currentSize++;
            doInsert(key, currentSize - 1);
            this.cache[currentSize - 1] = t;
        }
    }

    @Override
    public void write(Long key) {
        doWrite(key);
    }

    @Override
    public long hitCount() {
        return hitCount;
    }

    @Override
    public long missCount() {
        return missCount;
    }

    @Override
    public double hitRatio() {
        if (hitCount == 0 && missCount == 0)
            return 1.0;
        return (double) hitCount / (hitCount + missCount);
    }

    protected abstract int doRead(Long key);
    protected abstract void doWrite(Long key);
    protected abstract void doInsert(Long key, int value);
    protected abstract int doRemove();
}
