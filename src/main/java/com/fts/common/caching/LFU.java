package com.fts.common.caching;

import com.fts.common.queue.ComparableAndIndex;
import com.fts.common.queue.DefaultQueue;
import com.fts.common.queue.PriorityQueue;

import java.util.HashMap;
import java.util.Map;

public class LFU<T> extends AbstractCaching<T> {
    private final Map<Long, Node> nodeMap;
    private final PriorityQueue<Node> queue;

    public LFU(DataObject<T> dataObject) {
        super(dataObject);
        nodeMap = new HashMap<>();
        queue = new DefaultQueue<>();
    }

    public LFU(DataObject<T> dataObject, int cacheSize) {
        super(dataObject, cacheSize);
        nodeMap = new HashMap<>();
        queue = new DefaultQueue<>(cacheSize);
    }

    @Override
    protected int doRead(Long key) {
        Node node = nodeMap.get(key);
        if (node == null)
            return -1;
        return node.value;
    }

    @Override
    protected void doWrite(Long key) {
        Node node = nodeMap.get(key);
        if (node != null) {
            node.count++;
            node.time = System.currentTimeMillis();
            queue.updateQueue(node);
        }
    }

    @Override
    protected void doInsert(Long key, int value) {
        Node node = new Node(key, value);
        nodeMap.put(key, node);
        queue.enQueue(node);
    }

    @Override
    protected int doRemove() {
        Node removedNode = queue.deQueue();
        nodeMap.remove(removedNode.key);
        return removedNode.value;
    }

    static final class Node implements ComparableAndIndex<Node> {
        Long key;
        int value;
        int count = 1;
        long time;
        int index;

        public Node(Long key, int value) {
            this.key = key;
            this.value = value;
            this.time = System.currentTimeMillis();
            this.index = -1;
        }

        @Override
        public int compareTo(Node other) {
            int delta = this.count - other.count;
            if (delta == 0) {
                if (this.time > other.time)
                    return 1;
                else if (this.time < other.time)
                    return -1;
                else return 0;
            }
            return delta;
        }

        @Override
        public void setIndex(int index) {
            this.index = index;
        }

        @Override
        public int getIndex() {
            return this.index;
        }
    }
}
