package com.fts.common.queue;

public interface PriorityQueue<E extends ComparableAndIndex<E>> {
    E deQueue();
    void enQueue(E e);
    void updateQueue(E e);
    boolean isFull();
}
