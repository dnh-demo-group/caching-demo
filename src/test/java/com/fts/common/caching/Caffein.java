package com.fts.common.caching;

import com.fts.common.WalletData;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.common.cache.CacheBuilder;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ExecutionException;

public class Caffein {

    private static Caching<WalletData> caching;
    private static Cache<Long, WalletData> caffein;

    private static com.google.common.cache.Cache<Long, WalletData> guava;
    Random random = new Random();

    @Before
    public void init() throws InterruptedException {
        DataObject<WalletData> dataObject = new WalletData();
        caching = new LFU<>(dataObject, 1000);

        //
        caffein = Caffeine.newBuilder()
                .maximumSize(1000)
                .recordStats()
                .build();

        //
        guava = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .recordStats()
                .build();

        for (long i = 0; i < 600; i ++) {
            Thread.sleep(1);
            long k = random.nextInt(50) + 950;
            caching.insert(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
            caffein.put(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
            guava.put(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
        }
        for (long i = 0; i < 600; i ++) {
            Thread.sleep(1);
            long k = random.nextInt(2000) + 1;
            caching.insert(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
            caffein.put(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
            guava.put(k, new WalletData(k, BigDecimal.TEN, BigDecimal.ONE));
        }
    }

    private void ourRead(Long key) {
        caching.read(key);
        caching.insert(key, new WalletData(key, BigDecimal.TEN, BigDecimal.ONE));
    }
    private void theirRead(Long key) {
        caffein.getIfPresent(key);
        caffein.put(key, new WalletData(key, BigDecimal.TEN, BigDecimal.ONE));
    }
    private void guavaRead(Long key) {
        guava.getIfPresent(key);
        guava.put(key, new WalletData(key, BigDecimal.TEN, BigDecimal.ONE));
    }

    @Test
    public void compareTest() throws ExecutionException {
        for (int i = 0; i < 3000; i++) {
            int k = random.nextInt(50) + 950;
            ourRead((long)k);
            theirRead((long)k);
            guavaRead((long)k);
        }
        for (int i = 0; i < 3000; i++) {
            int k = random.nextInt(2000) + 1;
            ourRead((long)k);
            theirRead((long)k);
            guavaRead((long)k);
        }
        for (int i = 0; i < 3000; i++) {
            int k = random.nextInt(3000) + 1;
            ourRead((long)k);
            theirRead((long)k);
            guavaRead((long)k);
        }
        System.out.println("====================Our cache================");
        System.out.println(caching.hitCount());
        System.out.println(caching.missCount());
        System.out.println(caching.hitRatio());
        System.out.println("====================Caffein cache================");
        System.out.println(caffein.stats().hitCount());
        System.out.println(caffein.stats().missCount());
        System.out.println(caffein.stats().hitRate());
        System.out.println("====================Guava cache================");
        System.out.println(guava.stats().hitCount());
        System.out.println(guava.stats().missCount());
        System.out.println(guava.stats().hitRate());
    }

}
