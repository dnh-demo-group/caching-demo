package com.fts.common;

import com.fts.common.caching.DataObject;

import java.math.BigDecimal;

public class WalletData implements DataObject<WalletData> {

    private Long walletId;
    private BigDecimal balance;
    private BigDecimal freezeAmount;

    public WalletData() {
    }

    public WalletData(Long walletId, BigDecimal balance, BigDecimal freezeAmount) {
        this.walletId = walletId;
        this.balance = balance;
        this.freezeAmount = freezeAmount;
    }

    @Override
    public WalletData newInstance() {
        return new WalletData(-1L, BigDecimal.ZERO, BigDecimal.ZERO);
    }

    @Override
    public void set(WalletData oldData, WalletData newData) {
        oldData.walletId = newData.walletId;
        oldData.balance = newData.balance;
        oldData.freezeAmount = newData.freezeAmount;
    }

    @Override
    public String toString() {
        return "WalletData{" +
                "walletId=" + walletId +
                ", balance=" + balance +
                ", freezeAmount=" + freezeAmount +
                '}';
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
    }
}
