package com.fts.common.caching;

import com.fts.common.WalletData;
import com.google.common.cache.*;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

public class GuavaTest {

    @Test
    public void whenCacheMiss_thenValueIsComputed() throws ExecutionException {
        Cache<Long, WalletData> cache = CacheBuilder.newBuilder()
                .maximumSize(2)
                .recordStats()
                .build();
//        assertEquals(0, cache.size());

        WalletData walletData = cache.getIfPresent(1L);
        if (walletData == null) {
            cache.put(1L, new WalletData(1L, BigDecimal.TEN, BigDecimal.ZERO));
        }
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        walletData = cache.getIfPresent(1L);
        walletData.setBalance(walletData.getBalance().add(BigDecimal.valueOf(15L)));
        System.out.println(cache.getIfPresent(1L));
        System.out.println(cache.getIfPresent(1L));
        System.out.println(cache.getIfPresent(1L));
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        System.out.println(cache.getIfPresent(1L));
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        System.out.println(cache.getIfPresent(1L));
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        System.out.println(cache.getIfPresent(2L));
        cache.put(2L, new WalletData(2L, BigDecimal.TEN, BigDecimal.ZERO));
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        System.out.println(cache.getIfPresent(3L));
        cache.put(3L, new WalletData(3L, BigDecimal.TEN, BigDecimal.ZERO));
        System.out.println(cache.getIfPresent(2L));
        System.out.println(cache.stats().hitCount());
        System.out.println(cache.stats().missCount());
        System.out.println(cache.stats().hitRate());
    }
}
