package com.fts.common.queue;

public interface ComparableAndIndex<T> extends Comparable<T> {
    void setIndex(int index);
    int getIndex();
}
