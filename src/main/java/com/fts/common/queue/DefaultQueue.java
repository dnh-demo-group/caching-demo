package com.fts.common.queue;

public class DefaultQueue<E extends ComparableAndIndex<E>> implements PriorityQueue<E> {
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private int size;
    private Object[] queue;

    public DefaultQueue(int initialCapacity) {
        this.queue = new Object[initialCapacity];
    }

    public DefaultQueue() {
        this.queue = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    @Override
    @SuppressWarnings("unchecked")
    public E deQueue() {
        final E result;

        if ((result = (E) (queue[0])) != null) {
            final int n;
            final E x = (E) queue[(n = --size)];
            queue[n] = null;
            if (n > 0) {
                siftDown(0, x, n);
            }
        }
        return result;
    }

    @Override
    public void enQueue(E e) {
        int i = size;
        siftUp(i, e);
        size = i + 1;
    }

    @Override
    public void updateQueue(E e) {
        int pos = e.getIndex();
        siftDown(pos, e, size);
    }

    @Override
    public boolean isFull() {
        return size == queue.length;
    }

    @SuppressWarnings("unchecked")
    private void siftUp(int k, E e) {
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            E p = (E) queue[parent];
            if (e.compareTo(p) >= 0)
                break;
            queue[k] = p;
            p.setIndex(k);
            k = parent;
        }
        queue[k] = e;
        e.setIndex(k);
    }

    @SuppressWarnings("unchecked")
    private void siftDown(int k, E x, int n) {
        int half = n >>> 1;           // loop while a non-leaf
        while (k < half) {
            int child = (k << 1) + 1; // assume left child is least
            E c = (E) queue[child];
            int right = child + 1;
            if (right < n && c.compareTo((E) queue[right]) > 0) {
                c = (E) queue[child = right];
            }
            if (x.compareTo(c) <= 0)
                break;
            queue[k] = c;
            c.setIndex(k);
            k = child;
        }
        queue[k] = x;
        x.setIndex(k);
    }
}
