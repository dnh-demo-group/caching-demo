package com.fts.common.caching;

public interface DataObject<T> {
    T newInstance();
    void set(T oldData, T newData);
}
