package com.fts.common.caching;

public interface Caching<T> {
    T read(Long key);
    void write(Long key);
    void insert(Long key, T t);
    long hitCount();
    long missCount();
    double hitRatio();
}
